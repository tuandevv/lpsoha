$(document).ready(function () {
  $(".mobile-item").click(function () {
    var dropMenu = $(this).next(".drop-menu");
    $(".drop-menu").not(dropMenu).removeClass("active-sub");
    dropMenu.toggleClass("active-sub");
  });

  $(window).on("resize", function (e) {
    if ($(window).width() > 767) {
      $('#active').attr('checked', false);
      $(".drop-menu").removeClass("active-sub");
    }
  });

  // $('#active').change(function () {
  //   if (this.checked) {
  //     $('.introduct').addClass('active');
  //   } else {
  //     $('.introduct').removeClass('active');
  //   }
  // });

  $("#slide-planeta").slick({
    dots: false,
    arrows: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    variableWidth: 1,
    // autoplay: true,
    // autoplaySpeed: 2000,
  });

  $("#slide-memes").slick({
    dots: false,
    arrows: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    variableWidth: true,
    // autoplay: true,
    // autoplaySpeed: 2000,
  });
});


$("#btn-contact").on("click", function () {
  console.log(1);
  $('#modal-contact').show();
});
$("#closePopupContact").on("click", function () {
  $('#modal-contact').hide();
});
